# README #

### This is a training project for working with the database MySQL using JDBC ###

* This project implements the pattern of the DAO and the Factory.

### How set up? ###

* Create database MySQL
* Create table, use dbITcompanie\src\main\resources\initialize\initDB.sql
* Change database.properties, if you need this
* Run PMSystemRunner.java