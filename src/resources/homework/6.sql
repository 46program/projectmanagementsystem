SELECT developersProject.name_project, avg(salary) AS avarageSalary FROM developers
  INNER JOIN (SELECT smallestProfit.name_project, developers_projects.developer_id, developers_projects.project_id
              FROM developers_projects
                INNER JOIN ( SELECT id, name_project FROM projects
                GROUP BY id ORDER BY cost ASC LIMIT 1) AS smallestProfit
                  ON smallestProfit.id = developers_projects.project_id) AS developersProject
    ON developersProject.developer_id = developers.id;



