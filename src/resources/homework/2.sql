SELECT project_id, sum(salary) AS budget FROM developers
  INNER JOIN developers_projects ON developers.id = developers_projects.developer_id
GROUP BY project_id ORDER BY budget DESC LIMIT 1;