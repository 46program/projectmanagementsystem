ALTER TABLE developers ADD salary DECIMAL NOT NULL ;
/*
private String firstName;

private String lastName;

private String specialty;

private BigDecimal salary;

private Integer experience;

*/

UPDATE developers SET salary = 1000 WHERE id = 1;
UPDATE developers SET salary = 1500 WHERE id = 2;
UPDATE developers SET salary = 1300 WHERE id = 3;
UPDATE developers SET salary = 1450 WHERE id = 4;
UPDATE developers SET salary = 2000 WHERE id = 5;
