INSERT INTO skills (name_skill) VALUES ('C#');
INSERT INTO skills (name_skill) VALUES ('java');
INSERT INTO skills (name_skill) VALUES ('javaScript');
INSERT INTO skills (name_skill) VALUES ('UI/UX');
INSERT INTO skills (name_skill) VALUES ('Piton');
INSERT INTO skills (name_skill) VALUES ('Rubi');

INSERT INTO companies(name_companie) VALUES ('OLX');
INSERT INTO companies(name_companie) VALUES ('TFM');
INSERT INTO companies(name_companie) VALUES ('DMF');

INSERT INTO customers(name_customer) VALUES ('Jack');
INSERT INTO customers(name_customer) VALUES ('Bob');
INSERT INTO customers(name_customer) VALUES ('Steve');

INSERT INTO projects(name_project)
VALUES ('LK');
INSERT INTO projects(name_project)
VALUES ('DC');
INSERT INTO projects(name_project)
VALUES ('ND');
INSERT INTO projects(name_project)
VALUES ('MC');

INSERT INTO developers(name_developer)
VALUES ('Peter');
INSERT INTO developers(name_developer)
VALUES ('Andrei');
INSERT INTO developers(name_developer)
VALUES ('Konstantin');
INSERT INTO developers(name_developer)
VALUES ('Asya');
INSERT INTO developers(name_developer)
VALUES ('Peter');

INSERT INTO skills_developers(developer_id, skill_id) VALUES (1, 1);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (1, 2);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (2, 2);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (2, 3);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (3, 4);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (3, 5);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (4, 6);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (4, 1);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (5, 3);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (5, 6);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (5, 4);

INSERT INTO developers_projects(developer_id, project_id) VALUES (1, 1);
INSERT INTO developers_projects(developer_id, project_id) VALUES (2, 4);
INSERT INTO developers_projects(developer_id, project_id) VALUES (3, 2);
INSERT INTO developers_projects(developer_id, project_id) VALUES (4, 2);
INSERT INTO developers_projects(developer_id, project_id) VALUES (5, 3);
INSERT INTO developers_projects(developer_id, project_id) VALUES (2, 1);
INSERT INTO developers_projects(developer_id, project_id) VALUES (3, 4);

INSERT INTO projects_companies(project_id, companie_id, share) VALUES (1, 1, 2);
INSERT INTO projects_companies(project_id, companie_id, share) VALUES (1, 2, 2);
INSERT INTO projects_companies(project_id, companie_id, share) VALUES (2, 2, 1);
INSERT INTO projects_companies(project_id, companie_id, share) VALUES (3, 3, 1);
INSERT INTO projects_companies(project_id, companie_id, share) VALUES (4, 3, 1);

INSERT INTO projects_customers(project_id, customer_id) VALUES (1, 1);
INSERT INTO projects_customers(project_id, customer_id) VALUES (2, 2);
INSERT INTO projects_customers(project_id, customer_id) VALUES (3, 3);
INSERT INTO projects_customers(project_id, customer_id) VALUES (4, 3);
INSERT INTO projects_customers(project_id, customer_id) VALUES (1, 2);