CREATE TABLE IF NOT EXISTS skills (
  id   INT          NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name_skill VARCHAR(100) NOT NULL
)
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS companies (
  id   INT          NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name_companie VARCHAR(100) NOT NULL
)
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS customers (
  id   INT          NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name_customer VARCHAR(100) NOT NULL
)
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS developers (
  id   INT          NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name_developer VARCHAR(100) NOT NULL

)
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS projects (
  id   INT          NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name_project VARCHAR(100) NOT NULL
)
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS skills_developers (
  developer_id INT NOT NULL,
  skill_id     INT NOT NULL,
  CONSTRAINT uq_skills_developers UNIQUE (developer_id, skill_id),
  FOREIGN KEY (developer_id) REFERENCES developers (id),
  FOREIGN KEY (skill_id) REFERENCES skills (id)
)
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS developers_projects(
  developer_id INT NOT NULL,
  project_id   INT NOT NULL,
  CONSTRAINT uq_developers_projects UNIQUE (developer_id, project_id),
  FOREIGN KEY (developer_id) REFERENCES developers(id),
  FOREIGN KEY (project_id) REFERENCES  projects(id)
)
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS projects_companies (
  project_id   INT NOT NULL,
  companie_id  INT NOT NULL,
  share INT DEFAULT 100,
  CONSTRAINT uq_projects_companies UNIQUE (project_id, companie_id),
  FOREIGN KEY (project_id) REFERENCES projects (id),
  FOREIGN KEY (companie_id) REFERENCES companies (id)
)
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS projects_customers (
  project_id   INT NOT NULL,
  customer_id  INT NOT NULL,
  CONSTRAINT uq_projects_customers UNIQUE (project_id, customer_id),
  FOREIGN KEY (project_id) REFERENCES projects (id),
  FOREIGN KEY (customer_id) REFERENCES customers (id)
)
  ENGINE=InnoDB;