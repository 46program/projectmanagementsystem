package net.evgeniy46.pmsystem.dao;

import net.evgeniy46.pmsystem.model.Project;

/**
 * @author Evgenij Lukashik
 */
public interface ProjectDAO extends GenericDAO <Project, Long> {
}
