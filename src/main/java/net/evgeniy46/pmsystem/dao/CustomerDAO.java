package net.evgeniy46.pmsystem.dao;

import net.evgeniy46.pmsystem.model.Customer;

/**
 * @author Evgenij Lukashik
 */
public interface CustomerDAO extends GenericDAO <Customer, Long> {
}
