package net.evgeniy46.pmsystem.dao.hibernate;

import net.evgeniy46.pmsystem.dao.ProjectDAO;
import net.evgeniy46.pmsystem.model.Project;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.sql.SQLException;
import java.util.Collection;

/**
 * Class that provides a implementation of CRUD  operations for entity {@link Project}
 *
 * @author Evgenij Lukashik
 */
public class HibernateProjectDAOImp implements ProjectDAO {
    private SessionFactory sessionFactory;

    public HibernateProjectDAOImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(Project project) throws SQLException {
        try (Session session = sessionFactory.openSession()) {
            session.save(project);
        }

    }

    @Override
    public void update(Project project) throws SQLException {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.update(project);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }
        }

    }

    @Override
    public void remove(Project project) throws SQLException {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.delete(project);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }
        }
    }

    @Override
    public Collection<Project> getAll() {
        Collection<Project> projects = null;
        try (Session session = sessionFactory.getCurrentSession()) {
            try {
                session.beginTransaction();
                projects = session.createQuery("FROM Project ").list();
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }
        }
        return projects;
    }

    @Override
    public Project getById(Long id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Project.class, id);
        }
    }
}
