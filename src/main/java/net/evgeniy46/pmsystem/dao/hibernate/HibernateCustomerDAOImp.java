package net.evgeniy46.pmsystem.dao.hibernate;

import net.evgeniy46.pmsystem.dao.CustomerDAO;
import net.evgeniy46.pmsystem.model.Customer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.sql.SQLException;
import java.util.Collection;

/**
 * Class that provides a implementation of CRUD  operations for entity {@link Customer}
 *
 * @author Evgenij Lukashik
 */
public class HibernateCustomerDAOImp implements CustomerDAO {
    private SessionFactory sessionFactory;

    public HibernateCustomerDAOImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(Customer customer) throws SQLException {
        try (Session session = sessionFactory.openSession()) {
            session.save(customer);
        }

    }

    @Override
    public void update(Customer customer) throws SQLException {
        try(Session session = sessionFactory.openSession()){
            try{
                session.beginTransaction();
                session.update(customer);
                session.getTransaction().commit();
            }catch (Exception e){
                session.getTransaction().rollback();
            }
        }

    }

    @Override
    public void remove(Customer customer) throws SQLException {
        try(Session session = sessionFactory.openSession()) {
            try{
                session.beginTransaction();
                session.delete(customer);
                session.getTransaction().commit();
            }catch (Exception e){
                session.getTransaction().rollback();
            }

        }

    }

    @Override
    public Collection<Customer> getAll() {
        Collection<Customer> customers = null;
        try (Session session = sessionFactory.getCurrentSession()) {
            try {
                session.beginTransaction();
                customers = session.createQuery("FROM Customer ").list();
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }
        }
        return customers;
    }

    @Override
    public Customer getById(Long id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Customer.class, id);
        }
    }
}
