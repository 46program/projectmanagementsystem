package net.evgeniy46.pmsystem.dao.hibernate;

import net.evgeniy46.pmsystem.dao.DeveloperDAO;
import net.evgeniy46.pmsystem.model.Developer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.sql.SQLException;
import java.util.Collection;

/**
 * Class that provides a implementation of CRUD  operations for entity {@link Developer}
 *
 * @author Evgenij Lukashik
 */
public class HibernateDeveloperDAOImp implements DeveloperDAO {

    private SessionFactory sessionFactory;

    public HibernateDeveloperDAOImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(Developer developer) throws SQLException {
        try (Session session = sessionFactory.openSession()) {
            session.save(developer);
        }

    }

    @Override
    public void update(Developer developer) throws SQLException {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.update(developer);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }
        }
    }

    @Override
    public void remove(Developer developer) throws SQLException {
        try(Session session = sessionFactory.openSession()){
            try {
                session.beginTransaction();
                session.delete(developer);
                session.getTransaction().commit();
            }catch (Exception e){
                session.getTransaction().rollback();
            }
        }

    }

    @Override
    public Collection<Developer> getAll() {
        Collection<Developer> developers = null;
        try (org.hibernate.Session session = sessionFactory.getCurrentSession()) {
            try{
                session.beginTransaction();
                developers = session.createQuery("FROM Developer ").list();
                session.getTransaction().commit();
            }catch (Exception e){
                session.getTransaction().rollback();
            }

        }
        return developers;
    }

    @Override
    public Developer getById(Long id) {
        try(Session session =sessionFactory.openSession()){
            return session.get(Developer.class, id);
        }
    }
}
