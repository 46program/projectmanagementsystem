package net.evgeniy46.pmsystem.dao.hibernate;

import net.evgeniy46.pmsystem.dao.CompanyDAO;
import net.evgeniy46.pmsystem.model.Company;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.sql.SQLException;
import java.util.Collection;

/**
 * Class that provides a implementation of CRUD  operations for entity {@link Company}
 *
 * @author Evgenij Lukashik
 */
public class HibernateCompanyDAOImp implements CompanyDAO {
    private SessionFactory sessionFactory;

    public HibernateCompanyDAOImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(Company company) throws SQLException {
        try (Session session = sessionFactory.openSession()) {
            session.save(company);
        }

    }

    @Override
    public void update(Company company) throws SQLException {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.update(company);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }

        }
    }

    @Override
    public void remove(Company company) throws SQLException {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.delete(company);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }
        }

    }

    @Override
    public Collection<Company> getAll() {
        Collection<Company> companies = null;
        try (Session session = sessionFactory.getCurrentSession()) {
            try {
                session.beginTransaction();
                companies = session.createQuery("FROM Company").list();
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }
        }
        return companies;
    }

    @Override
    public Company getById(Long id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Company.class, id);
        }
    }
}
