package net.evgeniy46.pmsystem.dao.hibernate;

import net.evgeniy46.pmsystem.dao.SkillDAO;
import net.evgeniy46.pmsystem.model.Skill;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.sql.SQLException;
import java.util.Collection;

/**
 *Class that provides a implementation of CRUD  operations for entity {@link Skill}
 *
 * @author Evgenij Lukashik
 */
public class HibernateSkillDAOImp implements SkillDAO {

    private SessionFactory sessionFactory;

    public HibernateSkillDAOImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(Skill skill) throws SQLException {
        try(Session session = sessionFactory.openSession()){
            session.save(skill);
        }

    }

    @Override
    public void update(Skill skill) throws SQLException {
        try(Session session = sessionFactory.openSession()){
            try {
                session.beginTransaction();
                session.update(skill);
                session.getTransaction().commit();

            }catch (Exception e){
                session.getTransaction().rollback();
            }
        }

    }

    @Override
    public void remove(Skill skill) throws SQLException {
        try(Session session = sessionFactory.openSession()){
            try {
                session.beginTransaction();
                session.delete(skill);
                session.getTransaction().commit();
            }catch (Exception e){
                session.getTransaction().rollback();
            }
        }

    }

    @Override
    public Collection<Skill> getAll() {
        Collection<Skill> skills = null;
        try (org.hibernate.Session session = sessionFactory.getCurrentSession()) {
          try{
              session.beginTransaction();
              skills = session.createQuery("FROM Skill").list();
              session.getTransaction().commit();
          }catch (Exception e){
              session.getTransaction().rollback();
          }

        }
        return skills;
    }

    @Override
    public Skill getById(Long id) {
        try(Session session =sessionFactory.openSession()){
            return session.get(Skill.class, id);
        }
    }
}
