package net.evgeniy46.pmsystem.dao;

import net.evgeniy46.pmsystem.model.Developer;

/**
 * @author Evgenij Lukashik
 */
public interface DeveloperDAO extends GenericDAO <Developer,Long> {
}
