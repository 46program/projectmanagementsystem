package net.evgeniy46.pmsystem.dao.jdbc;

import net.evgeniy46.pmsystem.dao.CompanyDAO;
import net.evgeniy46.pmsystem.model.Company;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import static net.evgeniy46.pmsystem.util.ConnectionUtil.*;

/**
 * Implementation of {@link net.evgeniy46.pmsystem.dao.GenericDAO} interface for class {@link Company}.
 *
 * @author Evgenij Lukashik
 */
public class JdbcCompanyDAOImp implements CompanyDAO {
    private final static String INSERT_NEW = "INSERT INTO companies VALUES (?, ?, ?)";
    private final static String UPDATE_NEW = "UPDATE companies SET  name = ?, desckription = ? WHERE ID = ? ";
    private final static String DELETE = "DELETE FROM customers WHERE ID = ?";
    private final static String SHOW_ALL = "SELECT * FROM companies";
    public final static String GET_BY_ID = "SELECT * FROM companies WHERE ID = ?";


    public JdbcCompanyDAOImp() {
        try {
            getConnection();
        } catch (SQLException e) {
            throw new RuntimeException("Connection Failed" + e);
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException("Driver not found " + ex);
        }
    }


    @Override
    public void save(Company company) throws SQLException {
        try {
            preparedStatement = connection.prepareStatement(INSERT_NEW);
            preparedStatement.setLong(1, company.getId());
            preparedStatement.setString(2, company.getName());
            preparedStatement.setString(3, company.getDescription());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }

    }

    @Override
    public void update(Company company) throws SQLException {
        try {
            preparedStatement = connection.prepareStatement(UPDATE_NEW);
            preparedStatement.setString(1, company.getName());
            preparedStatement.setString(2, company.getDescription());
            preparedStatement.setLong(3, company.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }

    }

    @Override
    public void remove(Company company) throws SQLException {
        try {
            preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, company.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }

    }

    @Override
    public Collection<Company> getAll() {
        Collection<Company> list = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SHOW_ALL);
            while (resultSet.next()) {
                Company company = new Company();
                company.setId(resultSet.getLong(1));
                company.setName(resultSet.getString(2));
                company.setDescription(resultSet.getString(3));
                list.add(company);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public Company getById(Long id) {
        Company company = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_ID);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                company = new Company();
                company.setId(resultSet.getLong(1));
                company.setName(resultSet.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return company;
    }
}
