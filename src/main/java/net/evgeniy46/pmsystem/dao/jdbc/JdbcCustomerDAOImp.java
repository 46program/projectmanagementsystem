package net.evgeniy46.pmsystem.dao.jdbc;

import net.evgeniy46.pmsystem.dao.CustomerDAO;
import net.evgeniy46.pmsystem.model.Customer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import static net.evgeniy46.pmsystem.util.ConnectionUtil.*;

/**
 * Implementation of {@link net.evgeniy46.pmsystem.dao.GenericDAO} interface for class {@link Customer}.
 *
 * @author Evgenij Lukashik
 */
public class JdbcCustomerDAOImp implements CustomerDAO {

    private static final String INSERT_NEW = "INSERT INTO customers(ID, NAME, DESCRIPTION) VALUES (?, ?, ?)";
    private static final String GET_BY_ID = "SELECT * FROM customers WHERE ID = ?";
    private static final String UPDATE_NEW = "UPDATE customers SET NAME = ?, DESCRIPTION = ? WHERE ID = ?";
    private static final String DELETE = "DELETE FROM customers WHERE ID = ?";
    private static final String SHOW_ALL = "SELECT * FROM customers";

    public JdbcCustomerDAOImp() {
        try {
            getConnection();
        } catch (SQLException e) {
            throw new RuntimeException("Connection Failed" + e);
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException("Driver not found " + ex);
        }
    }

    @Override
    public void save(Customer customer) throws SQLException {
        try {
            preparedStatement = connection.prepareStatement(INSERT_NEW);
            preparedStatement.setLong(1, customer.getId());
            preparedStatement.setString(2, customer.getName());
            preparedStatement.setString(3, customer.getDescription());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }

    }

    @Override
    public void update(Customer customer) throws SQLException {
        try {
            preparedStatement = connection.prepareStatement(UPDATE_NEW);
            preparedStatement.setString(1, customer.getName());
            preparedStatement.setString(2, customer.getDescription());
            preparedStatement.setLong(3, customer.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }

    }

    @Override
    public void remove(Customer customer) throws SQLException {
        try {
            preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, customer.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }
    }

    @Override
    public Collection<Customer> getAll() {
        Collection<Customer> list = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SHOW_ALL);
            while (resultSet.next()) {
                Customer customer = new Customer();
                customer.setId(resultSet.getLong(1));
                customer.setName(resultSet.getString(2));
                list.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public Customer getById(Long id) {
        Customer customer = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_ID);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer();
                customer.setId(resultSet.getLong(1));
                customer.setName(resultSet.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return customer;
    }
}
