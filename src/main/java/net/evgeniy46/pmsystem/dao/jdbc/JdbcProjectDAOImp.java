package net.evgeniy46.pmsystem.dao.jdbc;

import net.evgeniy46.pmsystem.dao.ProjectDAO;
import net.evgeniy46.pmsystem.model.Project;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import static net.evgeniy46.pmsystem.util.ConnectionUtil.*;

/**
 * Implementation of {@link net.evgeniy46.pmsystem.dao.GenericDAO} interface for class {@link Project}.
 *
 * @author Evgenij Lukashik
 */
public class JdbcProjectDAOImp implements ProjectDAO {

    private final static String INSERT_NEW = "INSERT INTO projects VALUES (?, ?, ?)";
    private final static String UPDATE_NEW = "UPDATE projects SET  name = ?,description = ? WHERE ID = ? ";
    private final static String DELETE = "DELETE FROM projects WHERE ID = ?";
    private final static String SHOW_ALL = "SELECT * FROM projects";
    public final static String GET_BY_ID = "SELECT * FROM projects WHERE ID = ?";

    public JdbcProjectDAOImp() {
        try {
            getConnection();
        } catch (SQLException e) {
            throw new RuntimeException("Connection Failed" + e);
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException("Driver not found " + ex);
        }
    }

    @Override
    public void save(Project project) throws SQLException {
        try {
            preparedStatement = connection.prepareStatement(INSERT_NEW);
            preparedStatement.setLong(1, project.getId());
            preparedStatement.setString(2, project.getName());
            preparedStatement.setString(3, project.getDescription());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }

    }

    @Override
    public void update(Project project) throws SQLException {
        try {
            preparedStatement = connection.prepareStatement(UPDATE_NEW);
            preparedStatement.setString(1, project.getName());
            preparedStatement.setString(2, project.getDescription());
            preparedStatement.setLong(3, project.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }

    }

    @Override
    public void remove(Project project) throws SQLException {
        try {
            preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, project.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }

    }

    @Override
    public Collection<Project> getAll() {
        Collection<Project> list = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SHOW_ALL);
            while (resultSet.next()) {
                Project project = new Project();
                project.setId(resultSet.getLong(1));
                project.setName(resultSet.getString(2));
                project.setDescription(resultSet.getString(3));
                list.add(project);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public Project getById(Long id) {
        Project project = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_ID);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                project = new Project();
                project.setId(resultSet.getLong(1));
                project.setName(resultSet.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return project;
    }
}

