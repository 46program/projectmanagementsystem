package net.evgeniy46.pmsystem.dao.jdbc;

import net.evgeniy46.pmsystem.dao.DeveloperDAO;
import net.evgeniy46.pmsystem.model.Developer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import static net.evgeniy46.pmsystem.util.ConnectionUtil.*;

/**
 * Implementation of {@link net.evgeniy46.pmsystem.dao.GenericDAO} interface for class {@link Developer}.
 *
 * @author Evgenij Lukashik
 */
public class JdbcDeveloperDAOImp implements DeveloperDAO {
    private final static String INSERT_NEW = "INSERT INTO developers VALUES (?, ?, ?, ?, ?, ?, ?)";
    private final static String UPDATE_NEW = "UPDATE developers SET  first_name = ?, last_name=?, specialty = ?, experience = ?, salary = ?  WHERE ID = ? ";
    private final static String DELETE = "DELETE FROM developers WHERE ID = ?";
    private final static String SHOW_ALL = "SELECT * FROM developers";
    public final static String GET_BY_ID = "SELECT * FROM developers WHERE ID = ?";

    public JdbcDeveloperDAOImp() {

        try {
            getConnection();
        } catch (SQLException e) {
            throw new RuntimeException("Connection Failed" + e);
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException("Driver not found " + ex);
        }

    }

    @Override
    public void save(Developer developer) throws SQLException {
        try {
            preparedStatement = connection.prepareStatement(INSERT_NEW);
            preparedStatement.setLong(1, developer.getId());
            preparedStatement.setString(2, developer.getFirstName());
            preparedStatement.setString(3, developer.getLastName());
            preparedStatement.setString(4, developer.getSpecialty());
            preparedStatement.setInt(5, developer.getExperience());
            preparedStatement.setBigDecimal(6, developer.getSalary());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }

    }

    @Override
    public void update(Developer developer) throws SQLException {
        try {
            preparedStatement = connection.prepareStatement(UPDATE_NEW);
            preparedStatement.setString(1, developer.getFirstName());
            preparedStatement.setString(2, developer.getLastName());
            preparedStatement.setString(3, developer.getSpecialty());
            preparedStatement.setInt(3, developer.getExperience());
            preparedStatement.setBigDecimal(3, developer.getSalary());
            preparedStatement.setLong(6, developer.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    @Override
    public void remove(Developer developer) throws SQLException {
        try {
            preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, developer.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }
    }

    @Override
    public Collection<Developer> getAll() {
        Collection<Developer> list = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SHOW_ALL);
            while (resultSet.next()) {
                Developer developer = new Developer();
                developer.setId(resultSet.getLong(1));
                developer.setFirstName(resultSet.getString(2));
                developer.setLastName(resultSet.getString(3));
                developer.setSpecialty(resultSet.getString(4));
                developer.setExperience(resultSet.getInt(5));
                developer.setSalary(resultSet.getBigDecimal(6));

                list.add(developer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public Developer getById(Long id) {
        Developer developer = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_ID);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                developer = new Developer();
                developer.setId(resultSet.getLong(1));
                developer.setFirstName(resultSet.getString(2));
                developer.setLastName(resultSet.getString(3));
                developer.setSpecialty(resultSet.getString(4));
                developer.setExperience(resultSet.getInt(5));
                developer.setSalary(resultSet.getBigDecimal(6));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return developer;
    }
}

