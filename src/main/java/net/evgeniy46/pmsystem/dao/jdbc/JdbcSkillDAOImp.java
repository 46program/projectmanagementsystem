package net.evgeniy46.pmsystem.dao.jdbc;

import net.evgeniy46.pmsystem.dao.SkillDAO;
import net.evgeniy46.pmsystem.model.Skill;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import static net.evgeniy46.pmsystem.util.ConnectionUtil.*;

/**
 * Implementation of {@link net.evgeniy46.pmsystem.dao.GenericDAO} interface for class {@link Skill}.
 *
 * @author Evgenij Lukashik
 */
public class JdbcSkillDAOImp implements SkillDAO {
    private final static String INSERT_NEW = "INSERT INTO skills VALUES (?, ?)";
    private final static String UPDATE_NEW = "UPDATE skills SET  name = ? WHERE ID = ? ";
    private final static String DELETE = "DELETE FROM skills WHERE ID = ?";
    private final static String SHOW_ALL = "SELECT * FROM skills";
    public final static String GET_BY_ID = "SELECT * FROM skills WHERE ID = ?";


    public JdbcSkillDAOImp() {
        try {
            getConnection();
        } catch (SQLException e) {
            throw new RuntimeException("Connection Failed" + e);
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException("Driver not found " + ex);
        }
    }

    @Override
    public void save(Skill skill) throws SQLException {

        try {
            preparedStatement = connection.prepareStatement(INSERT_NEW);
            preparedStatement.setLong(1, skill.getId());
            preparedStatement.setString(2, skill.getName());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }
    }

    @Override
    public void update(Skill skill) throws SQLException {
        try {
            preparedStatement = connection.prepareStatement(UPDATE_NEW);
            preparedStatement.setString(2, skill.getName());
            preparedStatement.setLong(1, skill.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }
    }

    @Override
    public void remove(Skill skill) throws SQLException {

        try {
            preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, skill.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }

    }

    @Override
    public Collection<Skill> getAll() {
        Collection<Skill> list = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SHOW_ALL);
            while (resultSet.next()) {
                Skill skill = new Skill();
                skill.setId(resultSet.getLong(1));
                skill.setName(resultSet.getString(2));
                list.add(skill);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }


    @Override
    public Skill getById(Long id) {
        Skill skill = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_ID);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                skill = new Skill();
                skill.setId(resultSet.getLong(1));
                skill.setName(resultSet.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return skill;
    }
}
