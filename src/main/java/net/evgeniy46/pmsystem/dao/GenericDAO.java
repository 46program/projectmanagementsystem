package net.evgeniy46.pmsystem.dao;

import java.sql.SQLException;
import java.util.Collection;

/**
 * @author Evgenij Lukashik
 */
public interface GenericDAO<T, ID> {


    void save(T entity) throws SQLException;

    void update(T entity) throws SQLException;

    void remove(T entity) throws SQLException;

    Collection<T> getAll();

    T getById(ID id);
}
