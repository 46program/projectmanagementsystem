package net.evgeniy46.pmsystem.dao;

import net.evgeniy46.pmsystem.model.Company;

/**
 *@author Evgenij Lukashik
 */
public interface CompanyDAO extends GenericDAO <Company, Long> {
}
