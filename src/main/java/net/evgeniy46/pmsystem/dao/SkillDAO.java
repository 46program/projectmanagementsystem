package net.evgeniy46.pmsystem.dao;

import net.evgeniy46.pmsystem.model.Skill;

/**
 * @author Evgenij Lukashik
 */
public interface SkillDAO extends GenericDAO <Skill,Long> {
}
