package net.evgeniy46.pmsystem.util;


import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Configuration class that provide application with Database Connection.
 *
 * @author Eugene Suleimanov
 */
public class HibernateUtil {

    private static SessionFactory sessionFactory;

    static {
        try {

            sessionFactory = new Configuration().configure().buildSessionFactory();


        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;

    }

}
