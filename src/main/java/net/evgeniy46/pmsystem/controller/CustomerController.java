package net.evgeniy46.pmsystem.controller;

import net.evgeniy46.pmsystem.dao.hibernate.HibernateCustomerDAOImp;
import net.evgeniy46.pmsystem.model.Customer;
import net.evgeniy46.pmsystem.util.HibernateUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Controller that handles requests connected with {@link Customer}
 *
 * @author Evgenij Lukashik
 */
public class CustomerController implements Controller<Customer, Long> {
    HibernateCustomerDAOImp hibernateCustomerDAOImp = new HibernateCustomerDAOImp(HibernateUtil.getSessionFactory());

    @Override
    public void save(Customer customer) throws SQLException {
        hibernateCustomerDAOImp.save(customer);
    }

    @Override
    public void update(Customer customer) throws SQLException {
        hibernateCustomerDAOImp.update(customer);
    }

    @Override
    public void remove(Customer customer) throws SQLException {
        hibernateCustomerDAOImp.remove(customer);
    }

    @Override
    public Collection<Customer> getAll() {
        Collection<Customer> list = new ArrayList<>();
        return list = hibernateCustomerDAOImp.getAll();
    }

    @Override
    public Customer getById(Long id) {
        return hibernateCustomerDAOImp.getById(id);
    }
}
