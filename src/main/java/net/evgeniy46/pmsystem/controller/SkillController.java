package net.evgeniy46.pmsystem.controller;

import net.evgeniy46.pmsystem.dao.hibernate.HibernateSkillDAOImp;
import net.evgeniy46.pmsystem.model.Skill;
import net.evgeniy46.pmsystem.util.HibernateUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Controller that handles requests connected with {@link Skill}
 *
 * Created by Евгений on 31.05.2017.
 */
public class SkillController implements Controller<Skill, Long> {

    HibernateSkillDAOImp hibernateSkillDAOImp = new HibernateSkillDAOImp(HibernateUtil.getSessionFactory());

    @Override
    public void save(Skill skill) throws SQLException {
        hibernateSkillDAOImp.save(skill);
    }

    @Override
    public void update(Skill skill) throws SQLException {
        hibernateSkillDAOImp.update(skill);
    }

    @Override
    public void remove(Skill skill) throws SQLException {
        hibernateSkillDAOImp.remove(skill);
    }

    @Override
    public Collection<Skill> getAll() {
        Collection<Skill>list = new ArrayList<>();
        return list = hibernateSkillDAOImp.getAll();
    }

    @Override
    public Skill getById(Long id) {
        return hibernateSkillDAOImp.getById(id);
    }
}
