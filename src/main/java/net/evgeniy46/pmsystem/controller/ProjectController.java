package net.evgeniy46.pmsystem.controller;

import net.evgeniy46.pmsystem.dao.hibernate.HibernateProjectDAOImp;
import net.evgeniy46.pmsystem.model.Project;
import net.evgeniy46.pmsystem.util.HibernateUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Controller that handles requests connected with {@link Project}
 *
 * @author Evgenij Lukashik
 */
public class ProjectController implements Controller<Project, Long> {

    HibernateProjectDAOImp hibernateProjectDAOImp = new HibernateProjectDAOImp(HibernateUtil.getSessionFactory());

    @Override
    public void save(Project project) throws SQLException {
        hibernateProjectDAOImp.save(project);
    }

    @Override
    public void update(Project project) throws SQLException {
        hibernateProjectDAOImp.update(project);
    }

    @Override
    public void remove(Project project) throws SQLException {
        hibernateProjectDAOImp.remove(project);
    }

    @Override
    public Collection<Project> getAll() {
        Collection<Project> list = new ArrayList<>();
        return list = hibernateProjectDAOImp.getAll();
    }

    @Override
    public Project getById(Long id) {
        return hibernateProjectDAOImp.getById(id);
    }
}
