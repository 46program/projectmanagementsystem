package net.evgeniy46.pmsystem.controller;

import net.evgeniy46.pmsystem.dao.hibernate.HibernateDeveloperDAOImp;
import net.evgeniy46.pmsystem.model.Developer;
import net.evgeniy46.pmsystem.util.HibernateUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * \
 * Controller that handles requests connected with {@link Developer}
 *
 * @author Evgenij Lukashik
 */
public class DeveloperController implements Controller<Developer, Long> {

    HibernateDeveloperDAOImp hibernateDeveloperDAOImp = new HibernateDeveloperDAOImp(HibernateUtil.getSessionFactory());

    @Override
    public void save(Developer developer) throws SQLException {
        hibernateDeveloperDAOImp.save(developer);
    }

    @Override
    public void update(Developer developer) throws SQLException {
        hibernateDeveloperDAOImp.update(developer);
    }

    @Override
    public void remove(Developer developer) throws SQLException {
        hibernateDeveloperDAOImp.remove(developer);
    }

    @Override
    public Collection<Developer> getAll() {
        Collection<Developer> list = new ArrayList<>();
        return list = hibernateDeveloperDAOImp.getAll();
    }

    @Override
    public Developer getById(Long id) {
        return hibernateDeveloperDAOImp.getById(id);
    }
}
