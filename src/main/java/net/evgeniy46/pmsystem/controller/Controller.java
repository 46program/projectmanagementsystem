package net.evgeniy46.pmsystem.controller;

import java.sql.SQLException;
import java.util.Collection;

/**
 * Controller interface used as a based class for Controller classes
 *
 * @author Evgenij Lukashik
 */
public interface Controller<T, ID> {

    void save (T entity) throws SQLException;

    void update (T entity) throws SQLException;

    void remove (T entity) throws SQLException;

    Collection<T>getAll();

    T getById(ID id);

}
