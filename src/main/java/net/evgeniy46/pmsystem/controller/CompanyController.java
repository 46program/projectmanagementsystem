package net.evgeniy46.pmsystem.controller;

import net.evgeniy46.pmsystem.dao.hibernate.HibernateCompanyDAOImp;
import net.evgeniy46.pmsystem.model.Company;
import net.evgeniy46.pmsystem.util.HibernateUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Controller that handles requests from View connected with {@link Company}
 *
 * @author Evgenij Lukashik
 */
public class CompanyController implements Controller<Company, Long> {

    HibernateCompanyDAOImp hibernateCompanyDAOImp = new HibernateCompanyDAOImp(HibernateUtil.getSessionFactory());

    @Override
    public void save(Company company) throws SQLException {
        hibernateCompanyDAOImp.save(company);
    }

    @Override
    public void update(Company company) throws SQLException {
        hibernateCompanyDAOImp.update(company);
    }

    @Override
    public void remove(Company company) throws SQLException {
        hibernateCompanyDAOImp.remove(company);
    }

    @Override
    public Collection<Company> getAll() {
        Collection<Company> list = new ArrayList<>();
        return list = hibernateCompanyDAOImp.getAll();
    }

    @Override
    public Company getById(Long id) {
        return hibernateCompanyDAOImp.getById(id);
    }
}
