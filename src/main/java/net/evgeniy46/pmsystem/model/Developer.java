package net.evgeniy46.pmsystem.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

/**
 * Simple JavaBean domain object that represents a Developer.
 *
 * @author Evgenij Lukashik
 */
@Entity
@Table(name = "developers")
public class Developer extends NamedEntity {

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "specialty")
    private String specialty;

    @Column(name = "salary")
    private BigDecimal salary;

    @Column(name = "experience")
    private Integer experience;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }



    public Developer() {
    }
    @ManyToMany(cascade  = CascadeType.ALL)
    @JoinTable(
            name = "skills_developers",
            joinColumns = {@JoinColumn(name = "developer_id",referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "skill_id",referencedColumnName = "id")}
    )
    private Set<Skill> skills;

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    @Override
    public String toString() {
        return "Developer{" +
                "id='" + super.getId() + '\'' +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", salary='" + salary + '\'' +
                ", specialty='" + specialty + '\'' +
                ", experience='" + experience + '\'' +
                ", skills=" + skills +
                '}';
    }
}
