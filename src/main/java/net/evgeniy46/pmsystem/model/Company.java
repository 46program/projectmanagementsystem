package net.evgeniy46.pmsystem.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Simple JavaBean domain object that represents a Company
 *
 * @author Evgenij Lukashik
 */
@Entity
@Table(name = "companies")
public class Company extends NamedEntity {

    @Column(name = "description")
    private String description;



    @ManyToMany(cascade  = CascadeType.ALL)
    @JoinTable(
            name = "projects_companies",
            joinColumns = {@JoinColumn(name = "project_id",referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "companie_id",referencedColumnName = "id")}
    )
    private Set<Project> projects;

    public Company(Long id, String name, String description) {

    }

    public Company() {

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }



    @Override
    public String toString() {
        return "Company{" +
                "id='" + super.getId() + '\'' +
                "name='" + super.getName() + '\'' +
                "description='" + description + '\'' +
                ", projects=" + projects +
                '}';
    }
}
