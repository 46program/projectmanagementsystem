package net.evgeniy46.pmsystem.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Base class that extends class {@link BaseEntity} adding property 'name'.
 * Used as a base class for all objects that need this property.
 *
 * @author Evgenij Lukashik
 */
@MappedSuperclass
public class NamedEntity extends BaseEntity{

    @Column(name = "name")
    private String name;

    public NamedEntity() {
    }

    public NamedEntity(String name) {
        this.name = name;
    }

    public NamedEntity(Long id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
