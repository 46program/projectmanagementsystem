package net.evgeniy46.pmsystem.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Simple JavaBean domain object that represents a Customer
 *
 * @author Evgenij Lukashik
 */
@Entity
@Table(name = "customers")
public class Customer extends NamedEntity {

    @Column(name = "description")
    private String description;

    @ManyToMany(cascade  = CascadeType.ALL)
    @JoinTable(
            name = "projects_customers",
            joinColumns = {@JoinColumn(name = "project_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "customer_id", referencedColumnName = "id")}
    )
    private Set<Project> projects;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }


    @Override
    public String toString() {
        return "Customer{" +
                "id='" + super.getId() + '\'' +
                "name='" + super.getName() + '\'' +
                "description='" + description + '\'' +
                ", projects=" + projects +
                '}';
    }
}
