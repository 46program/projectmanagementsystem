package net.evgeniy46.pmsystem.model;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Set;

/**
 * Simple JavaBean domain object that represents a Skill.
 *
 * @author Evgenij Lukashik
 */
@Entity
@Table(name = "skills")
public class Skill extends NamedEntity {

    public Skill() {
    }

    @ManyToMany(mappedBy = "skills")
    private Set<Developer> developers;

    public Skill(String name) {
        super(name);
    }

    public Skill(Long id, String name) {
        super(id, name);
    }
}
