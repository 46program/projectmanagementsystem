package net.evgeniy46.pmsystem;

import net.evgeniy46.pmsystem.view.ConsoleHelper;

import java.io.IOException;
import java.sql.SQLException;

/** Main class of the project.
 *
 * @author Eugeniy Lukashik
 */
public class Runner {
    public static void main(String[] args) throws IOException, SQLException {
        ConsoleHelper consoleHelper = new ConsoleHelper();
        consoleHelper.consoleHelp();
    }
}
