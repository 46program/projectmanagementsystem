package net.evgeniy46.pmsystem.view;

import net.evgeniy46.pmsystem.controller.CustomerController;
import net.evgeniy46.pmsystem.model.Customer;
import net.evgeniy46.pmsystem.model.Project;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

import static net.evgeniy46.pmsystem.view.ConsoleHelper.*;

/**
 * View class that contains methods for user-console interface for entity {@link Customer}
 *
 *  @author Evgenij Lukashik
 */
public class CustomerView {

    private Set<Project> projects;

    Customer customer = new Customer();
    CustomerController customerController = new CustomerController();
    Long id;
    String name;
    String description;

    public void customerView() throws IOException, SQLException {
        writeMessage(" " +
                "1 - Add customer \n " +
                "2 - Remove customer \n " +
                "3 - Edit customer \n" +
                "4 - Search customer by ID \n" +
                "5 - Show all customer \n " +
                "6 - Show the customer's project\\n \n " +
                "7 - Exit on main menu \n");

        int choice = readInt();

        switch (choice)

        {
            case 1:
                writeMessage("\n Enter ID customer \n");
                id = readLong();
                writeMessage("Enter customer name \n");
                name = readString();
                writeMessage("\n Enter description \n");
                description = readString();
                writeMessage("\n Done \n");

                customer.setId(id);
                customer.setName(name);
                customer.setDescription(description);

                customerController.save(customer);
                break;

            case 2:
                writeMessage("\n Enter ID customer \n");
                id = readLong();
                customerController.remove(customerController.getById(id));
                writeMessage("\n Done \n");
                break;

            case 3:
                writeMessage("\n Enter ID customer \n");
                id = readLong();
                writeMessage("\n Enter new customer name \n");
                name = readString();
                writeMessage("\n Enter new description \n");
                description = readString();

                customer.setId(id);
                customer.setName(name);
                customer.setDescription(description);

                customerController.update(customer);
                System.out.println("\n Done \n");
                break;

            case 4:
                writeMessage("\n Enter ID customer \n");
                id = readLong();
                customerController.getById(id);
                System.out.println(customerController.getById(id));
                System.out.println("\n Done \n");
                break;

            case 5:
                writeMessage("Customer base: \n");
                customerController.getAll();
                System.out.println("\n Done \n");
                break;
            case 6:
                writeMessage("Enter id of customer:\n");
                id = readLong();
                showProjectsByCustomer();

            case 7:
                writeMessage("Go to the main menu...");
                ConsoleHelper consoleHelper = new ConsoleHelper();
                consoleHelper.consoleHelp();
                break;
            default:
                break;
        }

        customerView();
    }
    private void setHashSetProject() throws IOException {
        while (true) {
            Project project = new Project();
            writeMessage("Enter id of project:\n");
            project.setId(readLong());
            projects.add(project);
            writeMessage("Are you want add another one project?(yes - 1/ no - 2)\n");
            if (readInt() == 2) break;
        }
    }

    private void showProjectsByCustomer() {
        Customer customer;
        customer = customerController.getById(id);
        String projects = "";
        for (Project project :
                customer.getProjects()) {
            projects += project.getName() + ", ";
        }
        projects = projects.substring(0, projects.length() - 2);
        writeMessage("Developer:\n" +
                customer.getId() + " - " + customer.getName() +
                "(" + projects + ")\n");
    }
}

