package net.evgeniy46.pmsystem.view;

import net.evgeniy46.pmsystem.controller.SkillController;
import net.evgeniy46.pmsystem.model.Skill;

import java.io.IOException;
import java.sql.SQLException;

import static net.evgeniy46.pmsystem.view.ConsoleHelper.*;

/**
 *  View class that contains methods for user-console interface for entity {@link Skill}
 *
 *  @author Evgenij Lukashik
 *
 */
public class SkillView {

  Skill skill = new Skill();
    SkillController skillController = new SkillController();
    Long id;
    String name;

    public void skillView() throws IOException, SQLException {
        writeMessage(" " +
                "1 - Add\n" +
                "2 - Remove\n" +
                "3 - Update\n" +
                "4 - Search by ID\n" +
                "5 - Show all skills\n" +
                "6 - Return to main menu\n");


        int choice = readInt();

        switch (choice)

        {
            case 1:
                writeMessage("\n Enter ID skill \n");
                id = readLong();
                writeMessage("Enter skill name \n");
                name = readString();
                writeMessage("\n Done \n");

                skill.setId(id);
                skill.setName(name);


                skillController.save(skill);
                break;

            case 2:
                writeMessage("\n Enter ID skill \n");
                id = readLong();
                skillController.remove(skillController.getById(id));
                writeMessage("\n Done \n");
                break;

            case 3:
                writeMessage("\n Enter ID skill \n");
                id = readLong();
                writeMessage("\n Enter new skill name \n");
                name = readString();


                skill.setId(id);
                skill.setName(name);

                skillController.update(skill);
                System.out.println("\n Done \n");
                break;

            case 4:
                writeMessage("\n Enter ID skill \n");
                id = readLong();
                skillController.getById(id);
                System.out.println(skillController.getById(id));
                System.out.println("\n Done \n");
                break;

            case 5:
                writeMessage("Skill base: \n");
                skillController.getAll();
                System.out.println("\n Done \n");
                break;

            case 6:
                writeMessage("Go to the main menu...");
                ConsoleHelper consoleHelper = new ConsoleHelper();
                consoleHelper.consoleHelp();
                break;
            default:
                break;
        }

        skillView();
    }
}
