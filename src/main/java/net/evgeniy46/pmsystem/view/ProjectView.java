package net.evgeniy46.pmsystem.view;

import net.evgeniy46.pmsystem.controller.ProjectController;
import net.evgeniy46.pmsystem.model.Developer;
import net.evgeniy46.pmsystem.model.Project;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

import static net.evgeniy46.pmsystem.view.ConsoleHelper.*;

/**
 * View class that contains methods for user-console interface for entity {@link Developer}
 *
 *  @author Evgenij Lukashik
 */
public class ProjectView {

    private Set<Developer> developers;

    ProjectController projectController;
    Long id;
    String nameProj;
    String descrProj;


    public ProjectView() {

        projectController = new ProjectController();
    }

    public void projectView() throws IOException, SQLException {
        Project project = new Project();
        writeMessage("\n * * * Project * * *" + "\n" +
                "1 - Add project " +
                "2 - Remove project" +
                "3 - Edit project" +
                "4 - Search by ID " +
                "5 - Show all project " +
                "6 - Show the project's developer" +
                "7 - Exit to menu");

        int commandUser = readInt();

        switch (commandUser) {
            case 1:

                writeMessage("Note that the fields ID team,document and Company must match those already have. \n Enter please Id number of project: \n");
                id = readLong();
                writeMessage("Enter please name project: \n ");
                nameProj = readString();
                writeMessage("Enter please description project: \n");
                descrProj = readString();
                writeMessage("Please enter id of team: \n");
                saveProject(id, nameProj, descrProj);
                projectView();
                break;
            case 2:
                writeMessage("Enter please ID project to delete: \n");
                id = readLong();
                deleteProject(id);
                projectView();
                break;
            case 3:
                writeMessage("Note that the fields ID team,document and Company must match those already have. \n Enter please name project: \n");
                nameProj = readString();
                writeMessage("enter  description: \n");
                descrProj = readString();
                writeMessage("Please enter id of team: \n");

                writeMessage("enter ID project: \n");
                id = readLong();
                updateProject(nameProj, descrProj, id);
                projectView();
                break;
            case 4:
                writeMessage("Enter plese ID project: \n");
                id = readLong();
                searchProject(id);
                projectView();
                break;
            case 5:
                writeMessage("All projects:\n");
                projectController.getAll();
                projectView();
                break;
            case 6:
                writeMessage("Enter id of project:\n");
                id = readLong();
                showDevelopersByProject();
                break;
            case 7:
                writeMessage("Exiting to main menu \n");
                ConsoleHelper consoleHelper = new ConsoleHelper();
                consoleHelper.consoleHelp();
                break;

            default:
                break;

        }

    }

    public void saveProject(Long id, String name, String description) throws SQLException {
        Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        projectController.save(project);
        System.out.println("Done ");
    }

    public void deleteProject(Long id) throws SQLException {
        Project project = projectController.getById(id);
        projectController.remove(project);
        System.out.println("Done ");
    }

    public void updateProject(String name, String description, Long teamId) throws SQLException {
        Project project = projectController.getById(id);
        project.setName(name);
        project.setDescription(description);
        projectController.update(project);
        System.out.println("Done ");
    }

    public void searchProject(Long id) {
        Project project = projectController.getById(id);
        System.out.println(project);
        System.out.println("Done ");
    }

    private void showDevelopersByProject() {
        Project project;
        project = projectController.getById(id);
        String developers = "";
        for (Developer developer :
                project.getDevelopers()) {
            developers += developer.getName() + ", ";
        }
        developers = developers.substring(0, developers.length() - 2);
        writeMessage("Developer:\n" +
                project.getId() + " - " + project.getName() +
                "(" + developers + ")\n");
    }

    private void setHashSetDeveloper() throws IOException {
        while (true) {
            Developer developer = new Developer();
            writeMessage("Enter id of developer:\n");
            developer.setId(readLong());
            developers.add(developer);
            writeMessage("Are you want add another one developer?(yes - 1/ no - 2)\n");
            if (readInt() == 2) break;
        }
    }
}
