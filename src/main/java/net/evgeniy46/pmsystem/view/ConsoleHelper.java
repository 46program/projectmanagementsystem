package net.evgeniy46.pmsystem.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.SQLException;

/**
 * View class that contains methods for user-console interface
 *
 * @author Evgeniy Lukashik
 */
public class ConsoleHelper {

    public static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    private CompanyView companyView;
    private CustomerView customerView;
    private DeveloperView developerView;
    private ProjectView projectView;
    private SkillView skillView;

    public ConsoleHelper() {
        companyView = new CompanyView();
        customerView = new CustomerView();
        developerView = new DeveloperView();
        projectView = new ProjectView();
        skillView = new SkillView();

    }


    public static void writeMessage(String message) {
        System.out.println(message);
    }

    public void consoleHelp() throws IOException, SQLException {
        System.out.println("To perform CRUD operations, select the appropriate component, and press Enter: \n " +
                "1 Customer \n " +
                "2 Company \n " +
                "3 Project \n " +
                "4 Developer \n " +
                "5 Skill \n " +
                "6 Exit");
        int readChoice = readInt();
        switch (readChoice) {
            case 1:
                customerView.customerView();
                break;
            case 2:
                companyView.companyView();
                break;
            case 3:
                projectView.projectView();
                break;
            case 4:
                developerView.developerView();
                break;
            case 5:
                skillView.skillView();
                break;
            case 6:
                System.out.println("Exiting....");
                System.exit(0);
            default:
                break;
        }
    }

    public static int readInt() throws IOException {
        int number = 0;
        try {
            number = Integer.parseInt(bufferedReader.readLine());
        } catch (NumberFormatException e) {
            writeMessage("Incorrect data entered. Repeat input please.");
            readInt();
        }
        return number;
    }

    public static String readString() throws IOException {
        return bufferedReader.readLine();
    }
    public static Long readLong() throws IOException {
        Long number = null;
        try {
            number = Long.parseLong(bufferedReader.readLine());
        } catch (NumberFormatException e) {
            writeMessage("Incorrect data entered. Repeat input please.");
            readLong();
        }
        return number;
    }
    public static BigDecimal readBigDecimal() throws IOException {
        BigDecimal number = null;
        try {
            number = new BigDecimal(readLong());
        } catch (NumberFormatException e) {
            writeMessage("Incorrect data entered. Repeat input please.");
            readBigDecimal();
        }
        return number;
    }
}
