package net.evgeniy46.pmsystem.view;

import net.evgeniy46.pmsystem.controller.CompanyController;
import net.evgeniy46.pmsystem.model.Company;
import net.evgeniy46.pmsystem.model.Project;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

import static net.evgeniy46.pmsystem.view.ConsoleHelper.*;

/**
 * View class that contains methods for user-console interface for entity {@link Company}
 *
 *  @author Evgenij Lukashik
 */
public class CompanyView {
    private Set<Project> projects;

    public void companyView() throws IOException, SQLException {

        Company company = new Company();
        CompanyController companyController = new CompanyController();


        String name;
        Long id;
        String description = null;


        writeMessage("****  Company ****\n" +
                "1 - Add\n" +
                "2 - Remove\n" +
                "3 - Update\n4 - Search by ID\n" +
                "5 - Show the company's project\n" +
                "6 - Show all company\n" +
                "7 - Return to main menu\n");
        int commandNumber = readInt();

        switch (commandNumber) {
            case 1:
                writeMessage("Enter id of company:\n");
                id = readLong();
                writeMessage("Enter name of company:\n");
                name = readString();
                setHashSetProject();

                company.setId(id);
                company.setName(name);
                company.setDescription(description);

                companyController.save(company);
                break;
            case 2:
                writeMessage("Enter id of company:\n");
                id = readLong();
                companyController.remove(companyController.getById(id));
                writeMessage("\n Done \n");
                break;
            case 3:
                writeMessage("Enter id of company:\n");
                id = readLong();
                writeMessage("Enter name of company:\n");
                name = readString();
                setHashSetProject();

                company.setId(id);
                company.setName(name);
                company.setDescription(description);

                companyController.update(company);
                System.out.println("\n Done \n");
                break;
            case 4:
                writeMessage("Enter id of company:\n");
                id = readLong();
                companyController.getById(id);
                System.out.println(companyController.getById(id));
                System.out.println("\n Done \n");
                break;
            case 5:
                writeMessage("Enter id of company:\n");
                id = readLong();
                showProjectsByCompany();
                break;
            case 6:
                writeMessage("Customer base: \n");
                companyController.getAll();
                System.out.println("\n Done \n");
                break;
            case 7:
                writeMessage("Return to main menu\n");
                return;
            default:
                break;
        }
        companyView();
    }

    private void setHashSetProject() throws IOException {
        while (true) {
            Project project = new Project();
            writeMessage("Enter id of project:\n");
            project.setId(readLong());
            projects.add(project);
            writeMessage("Are you want add another one project?(yes - 1/ no - 2)\n");
            if (readInt() == 2) break;
        }
    }

    private void showProjectsByCompany() {
        Company company;
        Long id = null;
        CompanyController companyController = new CompanyController();
        company = companyController.getById(id);
        String projects = "";
        for (Project project :
                company.getProjects()) {
            projects += project.getName() + ", ";
        }
        projects = projects.substring(0, projects.length() - 2);
        writeMessage("Developer:\n" +
                company.getId() + " - " + company.getName() +
                "(" + projects + ")\n");
    }



}
