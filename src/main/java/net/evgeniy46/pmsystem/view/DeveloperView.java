package net.evgeniy46.pmsystem.view;

import net.evgeniy46.pmsystem.controller.DeveloperController;
import net.evgeniy46.pmsystem.model.Developer;
import net.evgeniy46.pmsystem.model.Skill;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Set;

import static net.evgeniy46.pmsystem.view.ConsoleHelper.*;

/**
 *View class that contains methods for user-console interface for entity {@link Developer}
 *
 *  @author Evgenij Lukashik
 */
public class DeveloperView {

    private Set<Skill> skills;

    Developer developer = new Developer();
    DeveloperController developerController = new DeveloperController();
    String data;
    Long id;
    Integer integer;
    BigDecimal salary;


    public void developerView() throws IOException, SQLException {
        writeMessage("* * * Developers * * *" + "\n" +
                "1 - Add \n" + "2 - Delete \n" + "3 - Update \n" + "4 - get by ID \n" + "5 - Show all developers \n" + "6 - Exit to the main menu \n");
        int commandNumber = readInt();

        switch (commandNumber) {
            case 1:
                writeMessage("Enter developer ID:\n");
                id = readLong();
                developer.setId(id);
                writeMessage("Enter developer name:\n");
                data = readString();
                developer.setFirstName(data);
                writeMessage("\nEnter developer surname:\n");
                data = readString();
                developer.setLastName(data);
                writeMessage("\nEnter developer salary:\n");
                salary = readBigDecimal();
                developer.setSalary(salary);
                writeMessage("\nDeveloper created!\n");
                developerController.save(developer);
                System.out.println(developerController.getById(id).toString());
                break;
            case 2:
                writeMessage("Enter developer ID:\n");
                id = readLong();
                developerController.remove(developerController.getById(id));
                writeMessage("\nDeveloper remove!\n");
                break;
            case 3:
                writeMessage("Enter developer ID:\n");
                id = readLong();
                developer.setId(id);
                writeMessage("Enter new name:\n");
                data = readString();
                developer.setFirstName(data);
                writeMessage("\nEnter new surname:\n");
                data = readString();
                developer.setLastName(data);
                writeMessage("\nEnter new salary:\n");
                salary = readBigDecimal();
                developer.setSalary(salary);
                writeMessage("\nEnter experience:\n");
                integer = readInt();
                developer.setExperience(integer);
                developerController.update(developer);
                writeMessage("\nUpdate complite!\n");
                break;
            case 4:
                writeMessage("Enter developer ID:\n");
                id = readLong();
                developerController.getById(id);
                System.out.println(developerController.getById(id).toString());
                break;
            case 5:
                developerController.getAll();
                break;
            case 6:
                writeMessage("Exit to the main menu:\n");
                ConsoleHelper consoleHelper = new ConsoleHelper();
                consoleHelper.consoleHelp();
            default:
                break;
        }
        developerView();
    }

    private void showSkillsByDeveloper() {
        Developer developer;
        developer = developerController.getById(id);
        StringBuilder skills = new StringBuilder();
        for (Skill skill :
                developer.getSkills()) {
            skills.append(skill.getName()).append(", ");
        }
        skills = new StringBuilder(skills.substring(0, skills.length() - 2));
        writeMessage("Developer:\n" +
                developer.getId() + " - " + developer.getName() +
                "(" + skills + ")\n");
    }

    private void setHashSetSkills() throws IOException {
        while (true) {
            Skill skill = new Skill();
            writeMessage("Enter id of skill:\n");
            skill.setId(readLong());
            skills.add(skill);
            writeMessage("Are you want add another one skill?(yes - 1/ no - 2)\n");
            if (readInt() == 2) break;
        }
    }
}

